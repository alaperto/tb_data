#!/bin/bash

#modify sensor pixel size
cp sensor_layouts_offline_December_1.cfg sensor_layouts_offline.cfg

#path to jobsub plus config and runlist names
JS='/afs/cern.ch/work/a/alaperto/TB_data/ilcsoft/v01-19-02/Eutelescope/v2.0.0/jobsub/jobsub.py  --config=config_December_1.cfg -csv runlist_general.csv -g'

#filename='runs.txt'

#for STEP in converter clustering hitmaker alignGBL1 alignGBL2 alignGBL3 fitGBL; do # complete, included align 
#for STEP in clustering hitmaker alignGBL1 alignGBL2 alignGBL3 fitGBL; do # complete, included align
for STEP in converter clustering hitmaker fitGBL; do #only reco, no align
#for STEP in fitGBL; do #only reco, no align
 
    for CURRENTRUN in {1831,1832,1833}; do
#    for CURRENTRUN in {1831,1832,1833}; do
	$JS $STEP $CURRENTRUN &
	#while read CURRENTRUN; do # reading each line
    done # < $filename  
    wait
done

#move output on eos
  cp tbtrack* ../tb_analysis/rawData_December_1/.
  mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/December_1/.
  mv output_December_1/lcio/* /eos/user/a/alaperto/tb_data/output_December_1/lcio/.
