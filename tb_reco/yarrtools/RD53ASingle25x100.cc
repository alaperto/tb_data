#include "RD53ASingle25x100.h"

namespace eutelescope {
  namespace geo {

    RD53ASingle25x100::RD53ASingle25x100()
        : EUTelGenericPixGeoDescr(20.00, 9.6, 0.015, // size X, Y, Z
                                  1, 400, 1, 192,      // min max X,Y
                                  93.660734)          // rad length
    {
      // Create the material for the sensor
      matSi =
          new TGeoMaterial("Si", 28.0855, 14.0, 2.33, -_radLength, 45.753206);
      Si = new TGeoMedium("RD53ASilicon", 1, matSi);

      /* Make a box for the sensitive area
      Size is: x=2*400+78*250=20300 microns and y=336*50=16800 microns
      MakeBox takes the half of those values in mm as arguments */
      plane = _tGeoManager->MakeBox("sns_rd53a", Si, 10.0, 4.8, 0.0075);

      //      auto row = plane->Divide("row", 2, 192, 0, 1, 0, "N");
      //      row->Divide("col", 1, 400, 0, 1, 0, "N");

      // in case x:y=100:25
      auto row = plane->Divide("row", 2, 384, 0, 1, 0, "N");
      row->Divide("col", 1, 200, 0, 1, 0, "N");
      // in case x:y=25:100
      //      auto row = plane->Divide("row", 2, 96, 0, 1, 0, "N");
      //      row->Divide("col", 1, 800, 0, 1, 0, "N");
    }

    RD53ASingle25x100::~RD53ASingle25x100() {
      // delete matSi;
      // delete Si;
    }

    void RD53ASingle25x100::createRootDescr(char const *planeVolume) {
      // Get the plane as provided by the EUTelGeometryTelescopeGeoDescription
      TGeoVolume *topplane = _tGeoManager->GetVolume(planeVolume);
      // Finaly add the sensitive area to the plane
      topplane->AddNode(plane, 1);
    }

    std::string RD53ASingle25x100::getPixName(int x, int y) {
      char buffer[100];
      // since pixel 0|0 is located on the upper left corner we have to correct
      // y by 192-y+1
      //(one for the offset in TGeo which starts counting at 1)
      //      snprintf(buffer, 100, "/sns_rd53a_1/row_%d/col_%d", 193 - y, x );

      if(x%2==1){
	snprintf(buffer, 100, "/sns_rd53a_1/row_%d/col_%d", 2*y -1, (x+1)/2 );
      }else{
	snprintf(buffer, 100, "/sns_rd53a_1/row_%d/col_%d", 2*y, x/2 );
      }

      // Return the full path
      return std::string(buffer);
    }

    // TODO: parse the path to a pixel number!
    std::pair<int, int> RD53ASingle25x100::getPixIndex(char const *) {
      return std::make_pair(0, 0);
    }

    EUTelGenericPixGeoDescr *maker() {
      RD53ASingle25x100 *mPixGeoDescr = new RD53ASingle25x100();
      return dynamic_cast<EUTelGenericPixGeoDescr *>(mPixGeoDescr);
    }

  } // namespace geo
} // namespace eutelescope
