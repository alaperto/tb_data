// Layout for a 100um thick 25x100 planar pixel sensor with odd connectivity on RD53a
// See img/layouts.png for a picture of the possible layouts
// Note: For 50x50 odd and odd connectivity are equivalent, while for 100x25 and 25x100 they are not

#ifndef RD53aPlanarSingle100um25x100odd_h
#define RD53aPlanarSingle100um25x100odd_h

// EUTELESCOPE
#include "EUTelGenericPixGeoDescr.h"

// ROOT
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoVolume.h"

namespace eutelescope {
  namespace geo {

    class RD53aPlanarSingle100um25x100odd : public EUTelGenericPixGeoDescr {

    public:
      RD53aPlanarSingle100um25x100odd();
      ~RD53aPlanarSingle100um25x100odd();

      void createRootDescr(char const *);
      std::string getPixName(int, int);
      std::pair<int, int> getPixIndex(char const *);

    protected:
      TGeoMaterial *matSi;
      TGeoMedium *Si;
      TGeoVolume *plane;
    };

    extern "C" {
    EUTelGenericPixGeoDescr *maker();
    }

  } // namespace geo
} // namespace eutelescope

#endif
