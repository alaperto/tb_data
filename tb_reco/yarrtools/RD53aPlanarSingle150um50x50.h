// Layout for a 150um thick 50x50 planar pixel sensor on RD53a
// See img/layouts.png for a picture of the possible layouts
// Note: For 50x50 even and odd connectivity are equivalent, while for 100x25 and 25x100 they are not

#ifndef RD53aPlanarSingle150um50x50_h
#define RD53aPlanarSingle150um50x50_h

// EUTELESCOPE
#include "EUTelGenericPixGeoDescr.h"

// ROOT
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoVolume.h"

namespace eutelescope {
  namespace geo {

    class RD53aPlanarSingle150um50x50 : public EUTelGenericPixGeoDescr {

    public:
      RD53aPlanarSingle150um50x50();
      ~RD53aPlanarSingle150um50x50();

      void createRootDescr(char const *);
      std::string getPixName(int, int);
      std::pair<int, int> getPixIndex(char const *);

    protected:
      TGeoMaterial *matSi;
      TGeoMedium *Si;
      TGeoVolume *plane;
    };

    extern "C" {
    EUTelGenericPixGeoDescr *maker();
    }

  } // namespace geo
} // namespace eutelescope

#endif
