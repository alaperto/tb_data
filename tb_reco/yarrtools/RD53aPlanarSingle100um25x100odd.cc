// Layout for a 100um thick 25x100 planar pixel sensor with odd connectivity on RD53a
// See img/layouts.png for a picture of the possible layouts
// Note: For 50x50 odd and odd connectivity are equivalent, while for 100x25 and 25x100 they are not

#include "RD53aPlanarSingle100um25x100odd.h"

namespace eutelescope {
  namespace geo {

    RD53aPlanarSingle100um25x100odd::RD53aPlanarSingle100um25x100odd()
        : EUTelGenericPixGeoDescr(20.00, 9.6, 0.100,// size X, Y, Z
                                  0, 399, 0, 191,    // min max X,Y
                                  93.660734)         // rad length
    {
      // Create the material for the sensor
      matSi =
          new TGeoMaterial("Si", 28.0855, 14.0, 2.33, -_radLength, 45.753206);
      Si = new TGeoMedium("RD53ASilicon", 1, matSi);

      // Make a box for the sensitive area
      plane = _tGeoManager->MakeBox("sns_rd53a", Si, 10.0, 4.8, 0.0075);

      auto row = plane->Divide("row", 2, 384, 0, 1, 0, "N");
      row->Divide("col", 1, 200, 0, 1, 0, "N");
    }

    RD53aPlanarSingle100um25x100odd::~RD53aPlanarSingle100um25x100odd() {
      // delete matSi;
      // delete Si;
    }

    void RD53aPlanarSingle100um25x100odd::createRootDescr(char const *planeVolume) {
      // Get the plane as provided by the EUTelGeometryTelescopeGeoDescription
      TGeoVolume *topplane = _tGeoManager->GetVolume(planeVolume);
      // Finaly add the sensitive area to the plane
      topplane->AddNode(plane, 1);
    }

    std::string RD53aPlanarSingle100um25x100odd::getPixName(int x, int y) {
      char buffer[100];
      if(x%2==1){
	snprintf(buffer, 100, "/sns_rd53a_1/row_%d/col_%d", 2*y+1 + 1, (x-1)/2 + 1); //(plus 1 for the offset in TGeo which starts counting at 1)
      }else{
	snprintf(buffer, 100, "/sns_rd53a_1/row_%d/col_%d", 2*y + 1, x/2 + 1); //(plus 1 for the offset in TGeo which starts counting at 1)
      }

      // Return the full path
      return std::string(buffer);
    }

    std::pair<int, int> RD53aPlanarSingle100um25x100odd::getPixIndex(char const *) {
      return std::make_pair(0, 0);
    }

    EUTelGenericPixGeoDescr *maker() {
      RD53aPlanarSingle100um25x100odd *mPixGeoDescr = new RD53aPlanarSingle100um25x100odd();
      return dynamic_cast<EUTelGenericPixGeoDescr *>(mPixGeoDescr);
    }

  } // namespace geo
} // namespace eutelescope
