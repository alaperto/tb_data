#ifndef RD53ASingle25x100_h
#define RD53ASingle25x100_h

/** @class RD53ASingle25x100
      * This class is the implementation of  @class EUTelGenericPixGeoDescr
      * for a default RD53A layout
  */

// EUTELESCOPE
#include "EUTelGenericPixGeoDescr.h"

// ROOT
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoVolume.h"

namespace eutelescope {
  namespace geo {

    class RD53ASingle25x100 : public EUTelGenericPixGeoDescr {

    public:
      RD53ASingle25x100();
      ~RD53ASingle25x100();

      void createRootDescr(char const *);
      std::string getPixName(int, int);
      std::pair<int, int> getPixIndex(char const *);

    protected:
      TGeoMaterial *matSi;
      TGeoMedium *Si;
      TGeoVolume *plane;
    };

    extern "C" {
    EUTelGenericPixGeoDescr *maker();
    }

  } // namespace geo
} // namespace eutelescope

#endif // RD53ASINGLE_H
