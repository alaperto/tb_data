// Layout for a 100um thick 100x25 planar pixel sensor with even connectivity on RD53a
// See img/layouts.png for a picture of the possible layouts
// Note: For 50x50 even and odd connectivity are equivalent, while for 100x25 and 25x100 they are not

#ifndef RD53aPlanarSingle100um100x25even_h
#define RD53aPlanarSingle100um100x25even_h

// EUTELESCOPE
#include "EUTelGenericPixGeoDescr.h"

// ROOT
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoVolume.h"

namespace eutelescope {
  namespace geo {

    class RD53aPlanarSingle100um100x25even : public EUTelGenericPixGeoDescr {

    public:
      RD53aPlanarSingle100um100x25even();
      ~RD53aPlanarSingle100um100x25even();

      void createRootDescr(char const *);
      std::string getPixName(int, int);
      std::pair<int, int> getPixIndex(char const *);

    protected:
      TGeoMaterial *matSi;
      TGeoMedium *Si;
      TGeoVolume *plane;
    };

    extern "C" {
    EUTelGenericPixGeoDescr *maker();
    }

  } // namespace geo
} // namespace eutelescope

#endif
