#modify sensor pixel size
cp sensor_layouts_offline_December_1.cfg sensor_layouts_offline.cfg

#for i in {2484..1585}
#for i in  1600 # 10 V 1000e

#for i in  {1831,1841} # 20-50 V 15-1000e
for i in  1831 # 20 V 1500-1000e
#for i in  1841 # 50 V 1500-1000e
#for i in  1845 # 70 V 1500-1000e

do
#  jobsub -c config_December_1.cfg -csv runlist_general.csv converter ${i}
  jobsub -c config_December_1.cfg -csv runlist_general.csv clustering ${i}
  jobsub -c config_December_1.cfg -csv runlist_general.csv hitmaker ${i}
  jobsub -c config_December_1.cfg -csv runlist_general.csv alignGBL1 ${i}
  jobsub -c config_December_1.cfg -csv runlist_general.csv alignGBL2 ${i}
  jobsub -c config_December_1.cfg -csv runlist_general.csv alignGBL3 ${i}
  jobsub -c config_December_1.cfg -csv runlist_general.csv fitGBL ${i}
  #move output on eos
  cp tbtrack* ../tb_analysis/rawData_December_1/.
  mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/December_1/.
  mv output_December_1/lcio/* /eos/user/a/alaperto/tb_data/output_December_1/lcio/.
done

