#modify sensor pixel size
cp sensor_layouts_offline_December_2.cfg sensor_layouts_offline.cfg

#for i in {2484..1585}
#for i in  1600 # 10 V 1000e

for i in {1824,1825,1828,1854,1855,1872,1873,1875,1877}
#for i in  1824 # 10 V 1500e LIN
#for i in  1855 # 80 V 1500e LIN 54-62 + 64-72
#for i in  1877 # 90 V 1500e LIN
#for i in  1875 # 100 V 1500e LIN

do
  jobsub -c config_December_2.cfg -csv runlist_general.csv converter ${i}
  jobsub -c config_December_2.cfg -csv runlist_general.csv clustering ${i}
  jobsub -c config_December_2.cfg -csv runlist_general.csv hitmaker ${i}
  #jobsub -c config_December_2.cfg -csv runlist_general.csv alignGBL1 ${i}
  #jobsub -c config_December_2.cfg -csv runlist_general.csv alignGBL2 ${i}
  #jobsub -c config_December_2.cfg -csv runlist_general.csv alignGBL3 ${i}
  #jobsub -c config_December_2.cfg -csv runlist_general.csv fitGBL ${i}
done
  #move output on eos
  cp tbtrack* ../tb_analysis/rawData_December_2/.
  #mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/December_2/.
  #mv output_December_2/lcio/* /eos/user/a/alaperto/tb_data/output_December_2/lcio/.

