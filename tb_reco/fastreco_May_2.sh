#!/bin/bash

#modify sensor pixel size
cp sensor_layouts_offline_May_2.cfg sensor_layouts_offline.cfg

#path to jobsub plus config and runlist names
JS='/afs/cern.ch/work/a/alaperto/TB_data/ilcsoft/v01-19-02/Eutelescope/v2.0.0/jobsub/jobsub.py  --config=config_May_2.cfg -csv runlist_general.csv -g'

#filename='runs.txt'

#for STEP in converter clustering hitmaker alignGBL alignGBL1 alignGBL2 fitGBL; do # complete, included align
#for STEP in  clustering hitmaker fitGBL; do #only reco, no align
for STEP in converter clustering hitmaker fitGBL; do #only reco, no align

    #2810,2812,2813,2814,2819,
    #2820,2821,2822,2823

    #2824,2826,2827,2828,2829,2830,2834,
    #2835,2836,2860,2861,2862,2863,2865,
    #2866,2867,2870,2872,2873,2874
 
    for CURRENTRUN in {2866,2867,2870,2872,2873,2874}; do
        $JS $STEP $CURRENTRUN &
	#while read CURRENTRUN; do # reading each line
    done # < $filename  
    wait
done

#move output on eos
mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/May_2/.
mv output_May_2/lcio/* /eos/user/a/alaperto/tb_data/output_May_2/lcio/.
mv output_May_2/histograms/* /eos/user/a/alaperto/tb_data/output_May_2/histograms/.
