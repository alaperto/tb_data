#modify sensor pixel size
cp sensor_layouts_offline_March.cfg sensor_layouts_offline.cfg

#for i in {2484..1585}
#for i in  1600 # 10 V 1000e

for i in  1609 # 5 V 1000e

#for i in  {1600,1602,1605,1607} # 10 V 1000e
#for i in  {1609,1610} # 5 V 1000e
#for i in  {1612,1613} # 3 V 1000e
#for i in  {1614,1615} # 2.5 V 1000e
#for i in  {1616,1617} # 2 V 1000e
#for i in  {1618,1620} # 1.5 V 1000e
#for i in  {1622,1623} # 1 V 1000e
#for i in  {1624,1626} # 0.5 V 1000e
#for i in  {1629,1630} # 0 V 1000e
#for i in  {1632,1634} # 15 V 1000e

#for i in  {1637,1638} # 15 V 1500e
#for i in  {1639,1640} # 10 V 1500e
#for i in  {1641,1643} # 5 V 1500e
#for i in  {1647,1650} # 3 V 1500e
#for i in  {1651,1652} # 2 V 1500e
#for i in  {1653,1654} # 1.5 V 1500e
#for i in  {1658,1659} # 1 V 1500e
#for i in  {1661,1665} # 0.5 V 1500e
#for i in  {1666,1668} # 0 V 1500e

#for i in  {1683,1685,1686} # 10 V 1000e 15deg
#for i in  {1687,1688} # 5 V 1000e 15deg
#for i in  {1689,1690} # 0 V 1000e 15deg

do
  jobsub -c config_March.cfg -csv runlist_general.csv converter ${i}
  jobsub -c config_March.cfg -csv runlist_general.csv clustering ${i}
  jobsub -c config_March.cfg -csv runlist_general.csv hitmaker ${i}
  #jobsub -c config_March.cfg -csv runlist_general.csv alignGBL1 ${i}
  #jobsub -c config_March.cfg -csv runlist_general.csv alignGBL2 ${i}
  #jobsub -c config_March.cfg -csv runlist_general.csv alignGBL3 ${i}
  jobsub -c config_March.cfg -csv runlist_general.csv fitGBL ${i}
  #move output on eos
  mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/March/.
  mv output_March/lcio/* /eos/user/a/alaperto/tb_data/output_March/lcio/.
done

