#!/bin/bash

if [ -z ${EUTELESCOPE+x} ]; 
then echo "$EUTELESCOPE is unset, have you sourced build_env.sh?";return;
else echo "EUTELESCOPE found in '$EUTELESCOPE'"; 
fi

#wget http://atlaspc5.kek.jp/pub/Main/HowToInstallEUTelescope/yarrtools.tar.gz
#tar -zxvf yarrtools.tar.gz

cp yarrtools/YarrConverterPlugin.cc $EUDAQ/main/lib/plugins/
cp yarrtools/RD53aPlanarSingle*.cc $EUTELESCOPE/geometries/src/. 
cp yarrtools/RD53aPlanarSingle*.h $EUTELESCOPE/geometries/include/.
cp yarrtools/RD53ASingle.cc $EUTELESCOPE/geometries/src 
cp yarrtools/RD53ASingle.h $EUTELESCOPE/geometries/include
cp yarrtools/RD53ASingle25x100.cc $EUTELESCOPE/geometries/src 
cp yarrtools/RD53ASingle25x100.h $EUTELESCOPE/geometries/include
echo -e "\e[91m..done!\e[39m"

echo -e "\e[91mRecompiling EUDAQ...\e[39m"
cd $EUTELESCOPE/external/eudaq/v1.x-dev/build
cmake ..
make -j4; make install;
cd -;
echo -e "\e[91m...done!\e[39m"

echo -e "\e[91mRecompiling EUTelescope...\e[39m"
cd $EUTELESCOPE/build
cmake ..
make -j4; make install;
cd -;
echo -e "\e[91m...done!\e[39m"

echo "Standard geometry set to: $GEO";

echo "following files are tested for run 535 reconstruction"
echo "yarrtools/gear_batch5.xml"
echo "yarrtools/config_rd53a.cfg"
echo "following files are tested for run 444 reconstruction"
echo "yarrtools/gear_batch4.xml"
echo "yarrtools/config_rd53aIFAE.cfg"

echo -e "\e[91m\e[5mALL DONE! Enjoy!\e[25m\e[39m"
