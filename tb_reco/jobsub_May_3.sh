#modify sensor pixel size
cp sensor_layouts_offline_May_3.cfg sensor_layouts_offline.cfg

for i in  2787

#Like May_2 but different alignment
#for i in {2787,2788,2789,2790,2792,2793,2795,2796,2797,2798,2799,2801,2802,2804,2807} #1-1.5 V 1500e

do
  #jobsub.py --config=config_May_3.cfg -csv runlist_general.csv converter ${i}
  #jobsub.py --config=config_May_3.cfg -csv runlist_general.csv clustering ${i}
  #jobsub.py --config=config_May_3.cfg -csv runlist_general.csv hitmaker ${i}
  #jobsub -c config_May_3.cfg -csv runlist_general.csv alignGBL1 ${i}
  #jobsub -c config_May_3.cfg -csv runlist_general.csv alignGBL2 ${i}
  #jobsub -c config_May_3.cfg -csv runlist_general.csv alignGBL3 ${i}
  jobsub -c config_May_3.cfg -csv runlist_general.csv fitGBL ${i}
  #move output on eos
  #mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/May_3/.
  #mv output_May_3/lcio/* /eos/user/a/alaperto/tb_data/output_May_3/lcio/.
done
