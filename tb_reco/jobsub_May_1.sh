#modify sensor pixel size
cp sensor_layouts_offline_May_1.cfg sensor_layouts_offline.cfg

#for i in  2640 # 10 V 1000e

#for i in {2635,2636,2637,2638,2640,2641,2642,2644,2645,2647,2649,2650,2652,2653,2654,2656,2657,2658,2659,2660,2661,2662,2664,2665,2667,2669,2671,2673,2674,2676,2677,2679,2680,2683,2684,2686,2687,2689,2690,2691,2693,2695,2697,2699,2701} #0-10 V 1000e

for i in {2703,2704,2705,2706,2707,2710,2712,2714,2716,2718,2719,2720,2723,2724,2751,2752,2753,2755,2757,2760,2762,2763,2764,2766,2768,2770,2771,2772,2774,2776,2777} #2-10 V 1500e

do
  jobsub.py --config=config_May_1.cfg -csv runlist_general.csv converter ${i}
  jobsub.py --config=config_May_1.cfg -csv runlist_general.csv clustering ${i}
  jobsub.py --config=config_May_1.cfg -csv runlist_general.csv hitmaker ${i}
  #jobsub -c config_May_1.cfg -csv runlist_general.csv alignGBL1 ${i}
  #jobsub -c config_May_1.cfg -csv runlist_general.csv alignGBL2 ${i}
  #jobsub -c config_May_1.cfg -csv runlist_general.csv alignGBL3 ${i}
  jobsub -c config_May_1.cfg -csv runlist_general.csv fitGBL ${i}
  #move output on eos
  mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/May_1/.
  mv output_May_1/lcio/* /eos/user/a/alaperto/tb_data/output_May_1/lcio/.
done

