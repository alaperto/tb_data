#modify sensor pixel size
cp sensor_layouts_offline_May_2.cfg sensor_layouts_offline.cfg

#for i in  2860 # 10 V 700e

# May 2
for i in {2810,2812,2813,2814} # 0 V 1500e

# May 2 10 V 770e
#for i in {2819,2820,2821,2822,2823,2824,2826,2827,2828,2829,2830,2834,2835,2836}

# May 2 700e
#for i in {2860,2861,2862,2863,2865,2866,2867,2870,2872,2873,2874} #3-5-10 V 700e

do
  jobsub.py --config=config_May_2.cfg -csv runlist_general.csv converter ${i}
  jobsub.py --config=config_May_2.cfg -csv runlist_general.csv clustering ${i}
  jobsub.py --config=config_May_2.cfg -csv runlist_general.csv hitmaker ${i}
  #jobsub -c config_May_2.cfg -csv runlist_general.csv alignGBL1 ${i}
  #jobsub -c config_May_2.cfg -csv runlist_general.csv alignGBL2 ${i}
  #jobsub -c config_May_2.cfg -csv runlist_general.csv alignGBL3 ${i}
  jobsub -c config_May_2.cfg -csv runlist_general.csv fitGBL ${i}
  #move output on eos
  mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/May_2/.
  mv output_May_2/lcio/* /eos/user/a/alaperto/tb_data/output_May_2/lcio/.
done
