#!/bin/bash

#modify sensor pixel size
cp sensor_layouts_offline_March_15.cfg sensor_layouts_offline.cfg

#path to jobsub plus config and runlist names
JS='/afs/cern.ch/work/a/alaperto/TB_data/ilcsoft/v01-19-02/Eutelescope/v2.0.0/jobsub/jobsub.py  --config=config_March_15.cfg -csv runlist_general.csv -g'

#filename='runs.txt'

#for STEP in converter clustering hitmaker alignGBL alignGBL1 alignGBL2 fitGBL; do # complete, included align
for STEP in converter clustering hitmaker fitGBL; do #only reco, no align
#for STEP in fitGBL; do #only reco, no align
    #for i in {2635,2636,2637,2638,2640,2641,2642,2644,2645,
    #          2647,2649,2650,2652,2653,2654,2656,2657,2658,
    #	       2659,2660,2661,2662,2664,2665,2667,2669,2671,
    #	       2673,2674,2676,2677,2679,2680,2683,2684,2686,
    #	       2687,2689,2690,2691,2693,2695,2697,2699,2701} #0-10 V 1000e

    #for i in  {1783,1785,1786} # 10 V 1000e 15deg
    #for i in  {1787,1788} # 5 V 1000e 15deg
    #for i in  {1789,1790} # 0 V 1000e 15deg

 
    for CURRENTRUN in {1786,1787,1788,1789,1790}; do
        $JS $STEP $CURRENTRUN &
	#while read CURRENTRUN; do # reading each line
    done # < $filename  
    wait
done

#move output on eos
mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/March_15/.
mv output_March_15/lcio/* /eos/user/a/alaperto/tb_data/output_March_15/lcio/.
