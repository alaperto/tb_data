 #modify sensor pixel size
cp sensor_layouts_offline_March_15.cfg sensor_layouts_offline.cfg

#for i in {2484..1585}
#for i in  1783 # 10 V 1000e 15 deg ZY

for i in  {1785,1786} # 10 V 1000e 15deg
#for i in  {1787,1788} # 5 V 1000e 15deg
#for i in  {1789,1790} # 0 V 1000e 15deg

do
  jobsub -c config_March_15.cfg -csv runlist_general.csv converter ${i}
  jobsub -c config_March_15.cfg -csv runlist_general.csv clustering ${i}
  jobsub -c config_March_15.cfg -csv runlist_general.csv hitmaker ${i}
  jobsub -c config_March_15.cfg -csv runlist_general.csv alignGBL1 ${i}
  jobsub -c config_March_15.cfg -csv runlist_general.csv alignGBL2 ${i}
  jobsub -c config_March_15.cfg -csv runlist_general.csv alignGBL3 ${i}
  jobsub -c config_March_15.cfg -csv runlist_general.csv fitGBL ${i}
  #move output on eos
  mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/March_15/.
  mv output_March_15/lcio/* /eos/user/a/alaperto/tb_data/output_March_15/lcio/.
done

