#!/bin/bash

#modify sensor pixel size
cp sensor_layouts_offline_May_3.cfg sensor_layouts_offline.cfg

#path to jobsub plus config and runlist names
JS='/afs/cern.ch/work/a/alaperto/TB_data/ilcsoft/v01-19-02/Eutelescope/v2.0.0/jobsub/jobsub.py  --config=config_May_3.cfg -csv runlist_general.csv -g'

#filename='runs.txt'

for STEP in alignGBL2 fitGBL; do # complete, included align

#for STEP in converter clustering hitmaker fitGBL; do #only reco, no align
    #2787,2788,2789,2790,2792,2793,2795,2796,
    #2797,2798,2799,2801,2802,2804,2807

    #for CURRENTRUN in {2788,2789,2790,2792,2793,2795,2796}; do
    #    $JS $STEP $CURRENTRUN &
	#while read CURRENTRUN; do # reading each line
    #done # < $filename  
    $JS $STEP 2787 &
    wait
done

#move output on eos
mv tbtrack* /eos/user/a/alaperto/tb_data/rawData/May_3/.
mv output_May_3/lcio/* /eos/user/a/alaperto/tb_data/output_May_3/lcio/.
