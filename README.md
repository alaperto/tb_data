# Package for testbeam data reconstruction and analysis

This package is intended to be used for reconstruction and analysis of Genova Testbeam Data (collected at DESY, electron beam).

The package is divided in two folders, since there are two steps in the procedure.

The first step is the reconstruction of tracks from DESY raw data to root Ntuples, done with EUTelescope (see Eutelescope installation).

The second step is the analysis of the Ntuples with TBmon2 (see TBmon2 installation).

## EUTelescope installation

Follow instructions here:

https://gitlab.cern.ch/atlas-genova/Laboratory/wikis/Testbeam-data-analysis-instructions#installation-of-eutelescope-v200

Then, inside the TB\_data folder (the main folder), install this package:

```
git clone https://gitlab.cern.ch/alaperto/tb_data.git

```

## Reconstruction code instructions

Inside the "tb_reco" folder there is the code that actually reconstruct the data, based on EUTelescope.

Go to the working directory and use this command to launch the reconstruction
```
cd tb_reco
source jobsub_March.sh
```

Inside the jobsub\_Ma... (March/May\_1/May\_2), there are instructions to launch multiple steps of the reconstruction together (Conversion/Clustering/Hits/Alignment/Fitter).

As an example, the command:
```
jobsub -c config_March.cfg -csv runlist_general.csv converter ${i}
```
will launch the Converter step of the March data (iterated on run number i, within the for loop).

The "runlist_general.csv" file includes the list of runs (to be filled).

The "config_March.cfg" file includes the parameters that are used during each of the reconstruction steps.

The geometrical information (position of the telescope planes) is included in the gear_Ma... folder.

The "gear.xml" file includes the starting information.

After each step, a subsequent gear file is generated (i.e. gear_pre.xml) that is used as imput for the following step of the reconstruction.

Thera are 5 steps in the reconstruction inside jobsub_Ma...

The steps alignGBL1/2/3 are deactivated since the alignment has been already done and the parameters are effective for each run of the datataking.

Please do not run alignGBL steps.

After the last step (fitGBL), ouput is copied in the eos folder:

/eos/user/a/alaperto/tb_data

The folder can be checked on CERNBOX:

https://cernbox.cern.ch/index.php/s/PqHdTji9SnJUapS
 
There are three data taking periods March (25x100 Genova),  May\_1 (50x50 Genova + Paris RD53A) and May\_2 (50x50 + Paris FEI4).

They are separated, since the geometry and alignment is different.

The numbers of the runs to be analyzed is included in these Git wikis.

Information on the March 2019 testbeam is here:
https://gitlab.cern.ch/atlas-genova/Laboratory/wikis/Testbeam-March-2019

Information on the May 2019 testbeam is here:
https://gitlab.cern.ch/atlas-genova/Laboratory/wikis/Testbeam-May-2019


## Analysis code instructions

Inside the TB\_data folder (the main folder), installed in the previous step, there is the tb\_analysis folder.

The folders are already organized.

To copy the root Ntuples files from eos (where they are stored after the reconstruction), do:

```
source scopy_eos_data_here
```

To launch the analysis execution (i.e. for the May\_1 case), do:

```
./tbmon2 OUTPUTPATH_May_1/
```

In case you need to create another output folder
./tbmon2 somewhere/OUTPUTPATH_XXX/

Then you need to modify the configuration files:

```
OUTPUTPATH_XXX/config/mainConfig.cfg
OUTPUTPATH_XXX/config/analysisConfig.cfg &
```

## TBmon2 installation (may be useless, already installed with this package)

Follow instructions here:

https://gitlab.cern.ch/atlas-genova/Laboratory/wikis/Testbeam-data-analysis-instructions#installation-of-tbmon2



