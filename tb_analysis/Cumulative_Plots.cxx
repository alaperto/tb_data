#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString macro_name = "hei";

  //TString list_name = "list/lista_March_1000.txt", DUT = "30", geo = "0"; int axis = 200, ayis = 384, ix=133, fx=196, iy=18, fy=359;
  //TString list_name = "list/lista_March_1500.txt", DUT = "30", geo = "0"; int axis = 200, ayis = 384, ix=133, fx=196, iy=18, fy=359;

  //TString list_name = "list/lista_May_1000.txt", DUT = "40", geo = "2"; int axis = 400, ayis = 192, ix=266, fx=395, iy=80, fy=188;
  //TString list_name = "list/lista_May_1500.txt", DUT = "40", geo = "2"; int axis = 400, ayis = 192, ix=266, fx=395, iy=80, fy=188;
  //TString list_name = "list/lista_May_700.txt", DUT = "40", geo = "2"; int axis = 400, ayis = 192, ix=266, fx=395, iy=80, fy=188;

  TString list_name = "list/lista_March_15.txt", DUT = "40", geo = "2"; int axis = 200, ayis = 384, ix=133, fx=197, iy=2, fy=344;
  //TString list_name = "list/lista_March_all.txt", DUT = "30", geo = "0"; int axis = 200, ayis = 384, ix=133, fx=196, iy=18, fy=359;
  //TString list_name = "list/lista_May_all.txt", DUT = "40", geo = "2"; int axis = 400, ayis = 192, ix=266, fx=395, iy=80, fy=188;

  int m = 0;
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  int qq =200;

  TCanvas *plot_T[qq];
  TCanvas *plot_H[qq];
  TCanvas *plot_E[qq];
  TCanvas *plot_A[qq];
  TCanvas *plot_MT[qq];
  TCanvas *plot_MH[qq];
  TCanvas *plot_ME[qq];
  TCanvas *plot_MA[qq];
  TH2D *Map_T[qq];
  TH2D *Map_H[qq];
  TH2D *Map_E[qq];
  TH2D *Map_A[qq];
  TH2D *Map_MT[qq];
  TH2D *Map_MH[qq];
  TH2D *Map_ME[qq];
  TH2D *Map_MA[qq];
  TH1D *h1[qq];

  TString histoT = "Efficiency/track_map_dut_"+DUT;
  TString histoH = "Efficiency/hit_map_dut_"+DUT;
  TString histoE = "Efficiency/efficiency_map_dut_"+DUT;
  TString histoA = "Efficiency/efficiency_map_dut_"+DUT;
  
  TString histoMT = "Efficiency/track_pixel_map_dut_"+DUT+"_geometry_"+geo;
  TString histoMH = "Efficiency/hit_pixel_map_dut_"+DUT+"_geometry_"+geo;
  TString histoME = "Efficiency/efficiency_pixel_map_dut_"+DUT+"_geometry_"+geo;
  TString histoMA = "Efficiency/efficiency_pixel_map_dut_"+DUT+"_geometry_"+geo;

  plot_MA[0] = new TCanvas("Canvas_2MA","Canvas_2MA",700,550);//550
  plot_ME[0] = new TCanvas("Canvas_2ME","Canvas_2ME",700,550);//550
  plot_A[0] = new TCanvas("Canvas_2A","Canvas_2A",700,550);//550
  plot_E[0] = new TCanvas("Canvas_2E","Canvas_2E",700,550);//550
  plot_E[190] = new TCanvas("Canvas_1","Canvas_1",700,550);//550
  Map_T[0] = new TH2D("Plot_2T","Plot_2T",axis,0,axis,ayis,0,ayis);//100x25
  Map_H[0] = new TH2D("Plot_2H","Plot_2H",axis,0,axis,ayis,0,ayis);//100x25
  Map_E[0] = new TH2D("Plot_2E","Plot_2E",axis,0,axis,ayis,0,ayis);//100x25
  Map_A[0] = new TH2D("Plot_2A","Plot_2A",axis,0,axis,ayis,0,ayis);//100x25
  Map_MT[0] = new TH2D("Plot_2MT","Plot_2MT",axis,0,axis,ayis,0,ayis);//100x25
  Map_MH[0] = new TH2D("Plot_2MH","Plot_2MH",axis,0,axis,ayis,0,ayis);//100x25
  Map_ME[0] = new TH2D("Plot_2ME","Plot_2ME",axis,0,axis,ayis,0,ayis);//100x25
  Map_MA[0] = new TH2D("Plot_2MA","Plot_2MA",axis,0,axis,ayis,0,ayis);//100x25
  h1[0] = new TH1D("1D_1","1D_1",100,0,101);

  TString num = "0";
  
  ifstream input2(list_name); 
  while(!input2.eof()){
    input2 >> macro_name;
    m++;
    
    if(macro_name!=""){


    h1[m] = new TH1D("1D"+macro_name,"1D"+macro_name,100,0,101);

    TFile *f = new TFile(macro_name);
    // Map_ = new TH2D("Plot_"+macro_name,"Plot_"+macro_name,400,0,400,192,0,192);//50x50
    //if(axis==400) Map_[m] = (TH2D*)f->Get("Efficiency/efficiency_map_dut_30");
    //else
    Map_T[m] = new TH2D("Plot_T_"+macro_name,"Plot_T_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
    Map_H[m] = new TH2D("Plot_H_"+macro_name,"Plot_H_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
    Map_E[m] = new TH2D("Plot_E_"+macro_name,"Plot_E_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
    Map_A[m] = new TH2D("Plot_A_"+macro_name,"Plot_A_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
 
    Map_MT[m] = new TH2D("Plot_MT_"+macro_name,"Plot_MT_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
    Map_MH[m] = new TH2D("Plot_MH_"+macro_name,"Plot_MH_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
    Map_ME[m] = new TH2D("Plot_ME_"+macro_name,"Plot_ME_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25
    Map_MA[m] = new TH2D("Plot_MA_"+macro_name,"Plot_MA_"+macro_name,axis,0,axis,ayis,0,ayis);//100x25

    num = "NamePlot_T_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_T[m] = (TH2D*)f->Get(histoT);
    Map_T[m]->SetName(num);
    Map_T[m]->SetTitle(num);
    
    num = "NamePlot_H_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_H[m] = (TH2D*)f->Get(histoH);
    Map_H[m]->SetName(num);
    Map_H[m]->SetTitle(num);
    
    num = "NamePlot_T-E_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_E[m] = (TH2D*)f->Get(histoH);
    Map_E[m]->SetName(num);
    Map_E[m]->SetTitle(num);

    num = "NamePlot_A_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_A[m] = (TH2D*)f->Get(histoE);
    Map_A[m]->SetName(num);
    Map_A[m]->SetTitle(num);

    num = "NamePlot_MT_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_MT[m] = (TH2D*)f->Get(histoMT);
    Map_MT[m]->SetName(num);
    Map_MT[m]->SetTitle(num);
    
    num = "NamePlot_MH_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_MH[m] = (TH2D*)f->Get(histoMH);
    Map_MH[m]->SetName(num);
    Map_MH[m]->SetTitle(num);
    
    num = "NamePlot_MT-E_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_ME[m] = (TH2D*)f->Get(histoMH);
    Map_ME[m]->SetName(num);
    Map_ME[m]->SetTitle(num);

    num = "NamePlot_MA_";
    num+=m;
    // cout<<"m "<<m<<" "<<macro_name<<" and "<<num<<endl;
    Map_MA[m] = (TH2D*)f->Get(histoME);
    Map_MA[m]->SetName(num);
    Map_MA[m]->SetTitle(num);



    if(m==1) Map_T[0] = (TH2D*)f->Get(histoT);
    if(m==1) Map_T[0]->SetName("CumulativeT");
    if(m==1) Map_T[0]->SetTitle("CumulativeT");
    if(m==1) Map_H[0] = (TH2D*)f->Get(histoH);
    if(m==1) Map_H[0]->SetName("CumulativeH");
    if(m==1) Map_H[0]->SetTitle("CumulativeH");
    if(m==1) Map_E[0] = (TH2D*)f->Get(histoH);
    if(m==1) Map_E[0]->SetName("CumulativeH");
    if(m==1) Map_E[0]->SetTitle("CumulativeH");
    if(m==1) Map_A[0] = (TH2D*)f->Get(histoA);
    if(m==1) Map_A[0]->SetName("CumulativeA");
    if(m==1) Map_A[0]->SetTitle("CumulativeA");
    if(m==1) Map_MT[0] = (TH2D*)f->Get(histoMT);
    if(m==1) Map_MT[0]->SetName("CumulativeMT");
    if(m==1) Map_MT[0]->SetTitle("CumulativeMT");
    if(m==1) Map_MH[0] = (TH2D*)f->Get(histoMH);
    if(m==1) Map_MH[0]->SetName("CumulativeMH");
    if(m==1) Map_MH[0]->SetTitle("CumulativeMH");
    if(m==1) Map_ME[0] = (TH2D*)f->Get(histoMH);
    if(m==1) Map_ME[0]->SetName("CumulativeMH");
    if(m==1) Map_ME[0]->SetTitle("CumulativeMH");
    if(m==1) Map_MA[0] = (TH2D*)f->Get(histoMA);
    if(m==1) Map_MA[0]->SetName("CumulativeMA");
    if(m==1) Map_MA[0]->SetTitle("CumulativeMA");
     
    Map_T[m]->GetXaxis()->SetTitle("Column");
    Map_T[m]->GetYaxis()->SetTitle("Row");
    // plot_T[m] = new TCanvas("Canvas_T_"+macro_name,"Canvas_T_"+macro_name,700,550);//550
    // Map_T[m]->Draw("COLZ");
    Map_H[m]->GetXaxis()->SetTitle("Column");
    Map_H[m]->GetYaxis()->SetTitle("Row");
    // plot_H[m] = new TCanvas("Canvas_H_"+macro_name,"Canvas_H_"+macro_name,700,550);//550
    // Map_H[m]->Draw("COLZ");

    double Tracks = Map_T[m]->Integral(0,axis,0,ayis);
    double Hits = Map_H[m]->Integral(0,axis,0,ayis);
    //cout<<"All   H/T "<<Hits<<"/"<<Tracks<<" = "<<Hits/Tracks<<endl;
    // cout<<"All   = "<<Hits/Tracks<<endl;
    double CTracks = Map_T[m]->Integral(ix,fx,iy,fy);
    double CHits = Map_H[m]->Integral(ix,fx,iy,fy);
    // cout<<"Clean = "<<CHits/CTracks<<endl;
    cout<<"  "<<CHits/CTracks<<endl;

    Map_E[m]->GetXaxis()->SetTitle("Column");
    Map_E[m]->GetYaxis()->SetTitle("Row");
    // plot_E[m] = new TCanvas("Canvas_E_"+macro_name,"Canvas_E_"+macro_name,700,550);//550
    // Map_E[m]->Draw("COLZ");
    Map_A[m]->GetXaxis()->SetTitle("Column");
    Map_A[m]->GetYaxis()->SetTitle("Row");
    // plot_A[m] = new TCanvas("Canvas_A_"+macro_name,"Canvas_A_"+macro_name,700,550);//550
    // Map_A[m]->Draw("COLZ");
    Map_MT[m]->GetXaxis()->SetTitle("Column");
    Map_MT[m]->GetYaxis()->SetTitle("Row");
    Map_MH[m]->GetXaxis()->SetTitle("Column");
    Map_MH[m]->GetYaxis()->SetTitle("Row");
    Map_ME[m]->GetXaxis()->SetTitle("Column");
    Map_ME[m]->GetYaxis()->SetTitle("Row");
    Map_MA[m]->GetXaxis()->SetTitle("Column");
    Map_MA[m]->GetYaxis()->SetTitle("Row");
   

    if(m>1){
      Map_T[0]->Add(Map_T[m]);
      Map_H[0]->Add(Map_H[m]);
      Map_E[0]->Add(Map_H[m]);
      Map_A[0]->Add(Map_A[m]);
      Map_MT[0]->Add(Map_MT[m]);
      Map_MH[0]->Add(Map_MH[m]);
      Map_ME[0]->Add(Map_ME[m]);
      Map_MA[0]->Add(Map_MA[m]);
    }
    Map_E[m]->Divide(Map_T[m]); //now can divide
    Map_ME[m]->Divide(Map_MT[m]); //now can divide
    
    }

  }

  double TracksALL = Map_T[0]->Integral(0,axis,0,ayis);
  double HitsALL = Map_H[0]->Integral(0,axis,0,ayis);
  cout<<"Total All   H/T "<<HitsALL<<"/"<<TracksALL<<" = "<<HitsALL/TracksALL<<endl;
  double CTracksALL = Map_T[0]->Integral(ix,fx,iy,fy);
  double CHitsALL = Map_H[0]->Integral(ix,fx,iy,fy);
  cout<<"Total Clear H/T "<<CHitsALL<<"/"<<CTracksALL<<" = "<<CHitsALL/CTracksALL<<endl;
    

  plot_MA[0]->cd();
  Map_MA[0]->Draw("COLZ");

  plot_ME[0]->cd();
  Map_ME[0]->Divide(Map_MT[0]);
  Map_ME[0]->Draw("COLZ");

  for(int x = ix; x < fx+1; x++)
    for(int y = iy; y < fy+1; y++)
      {
	Map_A[0]->SetBinContent(x+1,y+1,0);
      }  
  plot_A[0]->cd();
  Map_A[0]->Draw("COLZ");


  plot_E[0]->cd();
  Map_E[0]->Divide(Map_T[0]);
  Map_E[0]->Draw("COLZ");

  //clear lines
  for(int x = 1; x < ix+1; x++)
    for(int y = 1; y < ayis; y++)
      {
	Map_E[0]->SetBinContent(x+1,y+1,0);
    }
  for(int x = fx; x < axis; x++)
    for(int y = 1; y < ayis; y++)
      {
	Map_E[0]->SetBinContent(x+1,y+1,0);
      }
  for(int x = 1; x < axis; x++)
    for(int y = 1; y < iy+1; y++)
      {
	Map_E[0]->SetBinContent(x+1,y+1,0);
    }
  for(int x = 1; x < axis; x++)
    for(int y = fy; y < ayis; y++)
      {
	Map_E[0]->SetBinContent(x+1,y+1,0);
      }

  double tbc=1;
    
  for(int x = 1; x < axis; x++)
    for(int y = 1; y < ayis; y++)
      {
	tbc=Map_E[0]->GetBinContent(x+1,y+1);
	tbc*=100;
	if(tbc!=0)h1[0]->Fill(tbc);
      }  

  plot_E[190]->cd();
  h1[0]->Draw("HIST");

  app.Run(true);
  return 0;
}
