#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);
  TString PRINT = "Yes";
  TString fol = "./Tables/";

  TString M_name = "D11-3_", pixel = "25x100-1E";//--> F02_30_25x100-1E_D11.3
  TString threshold = "1500";
  TString final = "e_10-100V";
  TString do_B = "Yes"; //Yes if want to include also 2nd plot
  TString M_nameB = "D11-3_", pixelB = "25x100-1E";//--> F02_30_25x100-1E_D11.3
  TString thresholdB = "1000";
  TString finalB = "e_10-80V";
  TString do_C = "Yeso"; //Yes if want to include also 3rd plot
  TString M_nameC = "D11-3_", pixelC = "25x100-1E";//--> F02_30_25x100-1E_D11.3
  TString thresholdC = "1500";
  TString finalC = "e_10-100V";//unadded

  // TString M_name = "M2_", pixel = "25x100";//--> F02_91_02_25_1E
  // TString threshold = "1000";
  // TString final = "e_0-10V";
  // TString do_B = "Yes"; //Yes if want to include also 2nd plot
  // TString M_nameB = "M2_", pixelB = "25x100";//--> F02_91_02_25_1E
  // TString thresholdB = "1500";
  // TString finalB = "e_0-10V";
  // TString do_C = "Yeso"; //Yes if want to include also 3rd plot
  // TString M_nameC = "M2_", pixelC = "25x100";//--> F02_91_02_25_1E
  // TString thresholdC = "1000";
  // TString finalC = "e_0-10V_15deg";//unadded

  // TString M_name = "M3_", pixel = "50x50"; //--> F02_02_50_1E_03
  // TString threshold = "1000";
  // TString final = "e_0-10V";
  // TString do_B = "Yes"; //Yes if want to include also 2nd plot
  // TString M_nameB = "M3_", pixelB = "50x50";//--> F02_02_50_1E_03
  // TString thresholdB = "1500";
  // TString finalB = "e_0-10V";//missing 2 dots
  // TString do_C = "Yeso"; //Yes if want to include also 3rd plot
  // TString M_nameC = "M3_", pixelC = "50x50";//--> F02_02_50_1E_03
  // TString thresholdC = "700";
  // TString finalC = "e_3-10V";
  //  // TString M_nameC = "M2_", pixelC = "25x100";//--> F02_91_02_25_1E
  //  // TString thresholdC = "1000";
  //  // TString finalC = "e_0-15V";
  

  TString final_name = M_name+threshold;
  if(do_B=="Yes") final_name+="_"+M_nameB+thresholdB;
  if(do_C=="Yes") final_name+="_"+M_nameC+thresholdC;

  int ccc = 100;
  TString u[ccc], pl[ccc];
  Float_t v[ccc], ev[ccc], a[ccc], ea[ccc];
  int cc=0;

  cout<<fol<<M_name<<threshold<<final<<".txt"<< endl;

  ifstream input3(fol+M_name+threshold+final+".txt");//check voltages
  while(!input3.eof()){
    // //       Volt unc  Efficiency "+/-" unc
    // input3 >>v[cc]>>ev[cc]>>a[cc]>>pl[cc]>>ea[cc];
    //       Volt Efficiency
    input3 >>v[cc]>>a[cc];
    cout<<cc<<"  V  "<<v[cc]<<"  : effic.  "<<a[cc]<<endl;
    cc++;
  }

  cc--;//remove empty lastone
 
  TString uB[ccc], plB[ccc];
  Float_t vB[ccc], evB[ccc], aB[ccc], eaB[ccc];
  int ccB=0;
 
  cout<<fol<<M_nameB<<thresholdB<<finalB<<".txt"<< endl;

  ifstream input3B(fol+M_nameB+thresholdB+finalB+".txt");//check voltages
  while(!input3B.eof()){
    // //       Volt unc  Efficiency "+/-" unc
    // input3B >>vB[ccB]>>evB[ccB]>>aB[ccB]>>plB[ccB]>>eaB[ccB];
    //       Volt  Efficiency
    input3B >>vB[ccB]>>aB[ccB];
    cout<<ccB<<"  V  "<<vB[ccB]<<"  : effic.  "<<aB[ccB]<<endl;
    ccB++;
  }

  ccB--;//remove empty lastone
  TString uC[ccc], plC[ccc];
  Float_t vC[ccc], evC[ccc], aC[ccc], eaC[ccc];
  int ccC=0;
 
  ifstream input3C(fol+M_nameC+thresholdC+finalC+".txt");//check voltages
  while(!input3C.eof()){
    //       Volt unc  Efficiency "+/-" unc
    //input3C >>vC[ccC]>>evC[ccC]>>aC[ccC]>>plC[ccC]>>eaC[ccC];
    //       Volt Efficiency 
    input3C >>vC[ccC]>>aC[ccC];
    cout<<ccC<<"  V  "<<vC[ccC]<<"  : effic.  "<<endl;
    ccC++;
  }

  ccC--;//remove empty lastone

  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  // TGraphErrors *IV = new TGraphErrors(cc,&v[0],&a[0],&ev[0],&ea[0]);
  TGraph *IV = new TGraph(cc,&v[0],&a[0]);
  for (int i=0;i<IV->GetN();i++) IV->GetY()[i] *= 100; 
  IV->SetTitle(final_name);
  IV->GetYaxis()->SetTitle("Efficiency [%]");
  IV->GetXaxis()->SetTitle("Bias [V]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  IV->Draw("ALP");
  // TGraphErrors *IVB = new TGraphErrors(ccB,&vB[0],&aB[0],&evB[0],&eaB[0]);
  TGraph *IVB = new TGraph(ccB,&vB[0],&aB[0]);
  for (int i=0;i<IVB->GetN();i++) IVB->GetY()[i] *= 100; 
  IVB->GetYaxis()->SetTitle("Efficiency [%]");
  IVB->GetXaxis()->SetTitle("Bias [V]");
  IVB->SetMarkerStyle(21);
  IVB->SetMarkerColor(2);
  IVB->SetLineColor(2);
  if(do_B=="Yes") IVB->Draw("sameLP");
  // TGraphErrors *IVC = new TGraphErrors(ccC,&vC[0],&aC[0],&evC[0],&eaC[0]);
  TGraph *IVC = new TGraph(ccC,&vC[0],&aC[0]);
  for (int i=0;i<IVC->GetN();i++) IVC->GetY()[i] *= 100; 
  IVC->GetYaxis()->SetTitle("Efficiency [%]");
  IVC->GetXaxis()->SetTitle("Bias [V]");
  IVC->SetMarkerStyle(22);
  IVC->SetMarkerColor(3);
  IVC->SetLineColor(3);
  if(do_C=="Yes") IVC->Draw("sameLP");
  
  TLegend *leg2 = new TLegend(0.12,0.78,0.22,0.88);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
  leg2->AddEntry(IV,threshold+"e Lin FE ("+pixel+")");
  //leg2->AddEntry(IV,threshold+"e Diff FE ("+pixel+")");
  if(do_B=="Yes") leg2->AddEntry(IVB,thresholdB+"e Diff FE ("+pixelB+")");
  if(do_C=="Yes") leg2->AddEntry(IVC,thresholdC+"e Diff FE ("+pixelC+")");
  leg2->Draw("SAME");   

  //Change axis and redraw
  // IV->GetXaxis()->SetRangeUser(0,16);
  // IV->GetYaxis()->SetRangeUser(95,100);
  IV->GetXaxis()->SetRangeUser(0,110);
  IV->GetYaxis()->SetRangeUser(50,100);
  IV->Draw("ALP");
  if(do_B=="Yes") IVB->Draw("sameLP");
  if(do_C=="Yes") IVC->Draw("sameLP");
  leg2->Draw("SAME");   
  c3->Update();
  
  if(PRINT=="Yes") {
    TString plotName="Plots/"+final_name+"_Eff.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
