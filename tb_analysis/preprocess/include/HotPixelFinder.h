/* 
 * File:   HotPixelFinder.h
 * Author: daniel
 *
 * Created on 28. Januar 2014, 11:05
 */

#ifndef HOTPIXELFINDER_H
#define	HOTPIXELFINDER_H

#include "TBPreprocess.h"

class HotPixelFinder: public TBPreprocess
{
private:
	// one histogram for each dut
	std::map<IDEN, TH2D*> h_rawhitmap;
	std::map<IDEN, TH2D*> h_hitmap;
	std::map<IDEN, TH2D*> h_masked;
	std::map<IDEN, TH2D*> h_outOfTime;
	std::map<IDEN, TH1D*> h_lv1;
	std::map<IDEN, TH1D*> h_noiseOccupancy;
	std::map<IDEN, TH2I*> h_allMask;

	std::map<IDEN, int> eventCount;
	std::map<IDEN, int> lv1Min;
	std::map<IDEN, int> lv1Max;
	std::map<IDEN, double> maxOutOfTime;

	void checkDead(DUT* dut);
	void checkNoise(DUT* dut);
	void getAllMask(DUT* dut);

public:
	HotPixelFinder()
	{
		TBPreprocess::name = "HotPixelFinder";
	}
	
	virtual void init(TBCore* core);
	virtual void buildEvent(TBCore* core, TBEvent* event);
	virtual void finalize(TBCore* core);
};

// class factories
extern "C" TBPreprocess* create()
{
	return new HotPixelFinder;
}

extern "C" void destroy(TBPreprocess* tbp)
{
	delete tbp;
}

#endif	/* HOTPIXELFINDER_H */

