/* 
 * File:   ClusterFinder.h
 * Author: daniel
 *
 * Created on 28. Januar 2014, 00:28
 */

#ifndef CLUSTERFINDER_H
#define	CLUSTERFINDER_H

#include "TBPreprocess.h"

class ClusterFinder : public TBPreprocess
{
private:
  void hitCheck(TBCluster* cluster, TBHit* hit);
  void firstHitCheck(TBCluster* cluster, TBHit* hit);
  void findClusters(TBEvent* event);
public:
	ClusterFinder()
	{
    TBPreprocess::name = "ClusterFinder";
	}
	virtual void buildEvent(TBCore* core, TBEvent* event);
};

// class factories
extern "C" TBPreprocess* create()
{
	return new ClusterFinder;
}

extern "C" void destroy(TBPreprocess* tbp)
{
	delete tbp;
}

#endif	/* CLUSTERFINDER_H */

