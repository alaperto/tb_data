/* 
 * File:   TBAnalysis.h
 * Author: Daniel Kalin
 *
 * Created on 26. Januar 2014
 */

#ifndef TBANALYSIS_H
#define	TBANALYSIS_H

class TBAnalysis;
#include "TBEvent.h"
#include "TBCore.h"
#include "TBDut.h"

//#include "tbutils.h"
//#include "clusters.h"

#include <algorithm>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

// root
#include <TF1.h>

#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>

#include <TH1F.h>

#include <TFile.h>
#include <TTree.h>

#include <TCanvas.h>
#include <TEfficiency.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TGraph2D.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <THStack.h>
#include <TLegend.h>
#include <TMath.h>
#include <TPad.h>
#include <TPaletteAxis.h>
#include <TPaveStats.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TVector2.h>


class TBAnalysis
{
public:
	typedef int IDEN;
	
	std::string name;

	TBAnalysis()
	{ 
		TBAnalysis::name = "Unknown";
	};
	//Stuff to be done before anything else
	virtual void init(const TBCore* core) = 0;
	//How to process an event
	virtual void event(const TBCore* core, const TBEvent* event) = 0;
	//Stuff to be done after all events have been processed
	//virtual void finalize(const TbConfig &config) = 0;
	virtual void finalize(const TBCore* core) = 0;
	//Stuff to be done at the beginning of each run
	virtual void initRun(const TBCore* core){;}
	//Stuff to be done at the end of each run
	virtual void finalizeRun(const TBCore* core){;}
};

typedef TBAnalysis* createTBA_t();
typedef void destroyTBA_t(TBAnalysis*);

#endif	/* TBANALYSIS_H */

