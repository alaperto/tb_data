/* 
 * File:   Correlation.cc
 * Author: daniel
 * 
 * Created on 6. März 2014, 15:09
 */

#include "Correlation.h"

void Correlation::init(const TBCore* core)
{
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		int nCols = dut->getNcols();
		int nRows = dut->getNrows();
		double dutPitchX = dut->getDutPitchX();
		double dutPitchY = dut->getDutPitchY();

		Correlation::h_colvx[iden] = new TH2D("", ";Raw Hits Position [col];Track X [#mum]", nCols, -.5, nCols-.5, 64, 0, dutPitchX);
		Correlation::h_colvy[iden] = new TH2D("", ";Raw Hits Position [col];Track Y [#mum]", nCols, -.5, nCols-.5, 64, 0, dutPitchY);
		Correlation::h_rowvx[iden] = new TH2D("", ";Raw Hits Position [row];Track X [#mum]", nRows, -.5, nRows-.5, 64, 0, dutPitchX);
		Correlation::h_rowvy[iden] = new TH2D("", ";Raw Hits Position [row];Track Y [#mum]", nRows, -.5, nRows-.5, 64, 0, dutPitchY);
		Correlation::h_occup[iden] = new TH2D("", ";Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
	}
}

void Correlation::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden;
	
	if(event->fRawHits != kGood)
	{
		return;
	}
	
	if(event->fTracks != kGood)
	{
		return;
	}
	
	for(auto tbtrack: event->tracks)
	{
		double trackX = tbtrack->trackX;
		double trackY = tbtrack->trackY;
		
		for(auto tbhit: event->rawHits)
		{
			int col = tbhit->col;
			int row = tbhit->row;
			
			Correlation::h_colvx[iden]->Fill(col, trackX);
			Correlation::h_colvy[iden]->Fill(col, trackY);
			Correlation::h_rowvx[iden]->Fill(row, trackX);
			Correlation::h_rowvy[iden]->Fill(row, trackY);
			Correlation::h_occup[iden]->Fill(col, row);
		}
	}
}

void Correlation::finalize(const TBCore* core)
{
	core->output->processName = Correlation::name;
	
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		
		// set up cuts
		core->output->cuts = "only good tracks and raw hits";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Correlation Raw Hit Col Vs Track X DUT %i", iden);
		Correlation::h_colvx[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "colvx_dut_%i", iden);
		Correlation::h_colvx[iden]->SetName(histoTitle);
		core->output->drawAndSave(Correlation::h_colvx[iden], "colz", "emr");
		
		std::sprintf(histoTitle, "Correlation Raw Hit Col Vs Track Y DUT %i", iden);
		Correlation::h_colvy[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "colvy_dut_%i", iden);
		Correlation::h_colvy[iden]->SetName(histoTitle);
		core->output->drawAndSave(Correlation::h_colvy[iden], "colz", "emr");
		
		std::sprintf(histoTitle, "Correlation Raw Hit Row Vs Track X DUT %i", iden);
		Correlation::h_rowvx[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "rowvx_dut_%i", iden);
		Correlation::h_rowvx[iden]->SetName(histoTitle);
		core->output->drawAndSave(Correlation::h_rowvx[iden], "colz", "emr");
		
		std::sprintf(histoTitle, "Correlation Raw Hit Row Vs Track Y DUT %i", iden);
		Correlation::h_rowvy[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "rowvy_dut_%i", iden);
		Correlation::h_rowvy[iden]->SetName(histoTitle);
		core->output->drawAndSave(Correlation::h_rowvy[iden], "colz", "emr");
		
		std::sprintf(histoTitle, "Occup DUT %i", iden);
		Correlation::h_occup[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "occup_dut_%i", iden);
		Correlation::h_occup[iden]->SetName(histoTitle);
		core->output->drawAndSave(Correlation::h_occup[iden], "colz", "e");
		
	
		delete Correlation::h_colvx[iden];
		delete Correlation::h_colvy[iden];
		delete Correlation::h_rowvx[iden];
		delete Correlation::h_rowvy[iden];
		delete Correlation::h_occup[iden];
	}
	Correlation::h_colvx.clear();
	Correlation::h_colvy.clear();
	Correlation::h_rowvx.clear();
	Correlation::h_rowvy.clear();

	Correlation::h_occup.clear();
	
	delete [] histoTitle;
}