/* 
 * File:   EfficiencyVsGeometry.cc
 * Author: mareike
 * 
 * Created on 15. August 2017
 */

#include "EfficiencyVsGeometry.h"


void EfficiencyVsGeometry::init(const TBCore* core )
{
	for (auto dut:core->usedDUT)
	{
		int iden = dut->iden;

		for (int geometryNr = 0; geometryNr< dut->pixelGeometries.size(); geometryNr++)
		{
			// if (dut->pixelGeometries[geometryNr]->isEdgePixel()==true) continue;
			for (auto currentRun: core->tbconfig->rawDataRuns)
			{
				EfficiencyVsGeometry::tracks[iden][geometryNr][currentRun]=0.;
				EfficiencyVsGeometry::hits[iden][geometryNr][currentRun]=0.;
				EfficiencyVsGeometry::eff[iden][geometryNr][currentRun]=0.;
				EfficiencyVsGeometry::eeff[iden][geometryNr][currentRun]=0.;
			}
            EfficiencyVsGeometry::eff_allruns[iden][geometryNr]=0.;
            EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].first=0.;
            EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].second=0.;
		}

		std::string param = core->getParam(iden, EfficiencyVsGeometry::name, "effPlotMinY");
		if(param.compare("") != 0)
		{
			EfficiencyVsGeometry::effPlotMinY[iden] = std::stod(param);
		}
		else // default value
		{
			EfficiencyVsGeometry::effPlotMinY[iden] = 0.9;
		}

		std::string param2 = core->getParam(iden, EfficiencyVsGeometry::name, "showRunInfo");
		if(param2.compare("") != 0)
		{
			EfficiencyVsGeometry::showRunInfo[iden] = std::stod(param2);
		}
		else // default value
		{
			EfficiencyVsGeometry::showRunInfo[iden] = 0;
        }


	}
}

void EfficiencyVsGeometry::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden;
	int currentRun = core->currentRun;

	if (event->fTracks != kGood)
	{
		return;
	}

	for (auto tbtrack: event->tracks)
	{
		if(tbtrack->fTrackMaskedRegion == kGood)
		{
			continue;
		}
		
		// if(tbtrack->fTrackEdgeRegion == kGood)
		// {
		// 	continue;
		// }

                int trackCol = tbtrack->trackCol;
                int trackRow = tbtrack->trackRow;

                PixelGeometry* pixelGeometry = dut->pixelArray[trackCol][trackRow].getGeometry();
                int geometryNr = dut->getGeometryNumber(pixelGeometry);

		EfficiencyVsGeometry::tracks[iden][geometryNr][currentRun]++;
			
		if(tbtrack->fMatchedCluster == kBad)
		{
			continue;
		}

                EfficiencyVsGeometry::hits[iden][geometryNr][currentRun]++;	
	}
}

void EfficiencyVsGeometry::finalize(const TBCore* core)
{
	core->output->processName = EfficiencyVsGeometry::name;

	
	char* histoTitle = new char[500];

	for (auto dut: core->usedDUT)
	{
		int iden = dut->iden;

		// set up cuts
		core->output->cuts = "";
		core->output->currentPreprocessCuts = "";
		// -----------

		EfficiencyVsGeometry::efficiencyPerGeometry(dut, core);

		int nRuns = core->tbconfig->rawDataRuns.size();
		double run [nRuns];
		double erun [nRuns];
		double effRun [nRuns];
		double eeffRunUp [nRuns];
		double eeffRunLow [nRuns];

		int nGeo = dut->pixelGeometries.size();
        std::vector<double> geo;
        std::vector<double> effGeo;
        std::vector<double> egeo;
        std::vector<double> eeffGeo_lo;
        std::vector<double> eeffGeo_up;

        double nTracks[dut->pixelGeometries.size()];

        for (int geometryNr = 0; geometryNr<nGeo; geometryNr++)
        {
            // if (dut->pixelGeometries[geometryNr]->isEdgePixel()==true) continue;

            if (EfficiencyVsGeometry::eff_allruns[iden][geometryNr] > 0)
            {
                geo.push_back(geometryNr);
                egeo.push_back(0.0);
                effGeo.push_back(EfficiencyVsGeometry::eff_allruns[iden][geometryNr]);
                eeffGeo_lo.push_back(EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].first);
                eeffGeo_up.push_back(EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].second);
            }

            nTracks[geometryNr] = 0.;

            for (int irun =0; irun<nRuns;irun++)
            {
                run[irun] = core->tbconfig->rawDataRuns.at(irun);
                erun[irun] = 0.0;
                effRun[irun] = EfficiencyVsGeometry::h_EffVsRunFCP[iden][geometryNr]->GetEfficiency(irun+1);
                eeffRunUp[irun] = EfficiencyVsGeometry::h_EffVsRunFCP[iden][geometryNr]->GetEfficiencyErrorUp(irun+1);
                eeffRunLow[irun] = EfficiencyVsGeometry::h_EffVsRunFCP[iden][geometryNr]->GetEfficiencyErrorLow(irun+1);
                nTracks[geometryNr] += EfficiencyVsGeometry::tracks[iden][geometryNr][core->tbconfig->rawDataRuns.at(irun)];
            }
            EfficiencyVsGeometry::h_EffVsRun[iden][geometryNr]= new TGraphAsymmErrors(nRuns, &run[0], &effRun[0], &erun[0], &erun[0], &eeffRunLow[0], &eeffRunUp[0]);
            std::sprintf(histoTitle, "eff_vs_run_dut_%i_geo_%i", iden, geometryNr);
            EfficiencyVsGeometry::h_EffVsRun[iden][geometryNr] -> SetName(histoTitle);

            std::sprintf(histoTitle, "Efficiency vs Run DUT %i Geometry %i;Run;Efficiency", iden, geometryNr);
            EfficiencyVsGeometry::h_EffVsRun[iden][geometryNr] -> SetTitle(histoTitle);
            // EfficiencyVsGeometry::h_EffVsRun[iden][geometryNr] -> GetXaxis() -> SetRangeUser(run[0]-.5, run[nRuns-1]+.5);
            // EfficiencyVsGeometry::h_EffVsRun[iden][geometryNr] -> GetYaxis() -> SetRangeUser(EfficiencyVsGeometry::effPlotMinY[iden], 1.01);
            core->output->drawAndSave(EfficiencyVsGeometry::h_EffVsRun[iden][geometryNr], "AP", "");
        }

        EfficiencyVsGeometry::h_EffGeoNr[iden] = new TGraphAsymmErrors(geo.size(), &geo[0], &effGeo[0], &egeo[0], &egeo[0], &eeffGeo_lo[0], &eeffGeo_up[0]);

        std::sprintf(histoTitle, "Efficiency All Tracks Vs Geometry DUT %i runs %i-%i", iden, (int)run[0], (int)run[nRuns-1]);
        EfficiencyVsGeometry::h_EffGeoNr[iden] -> SetTitle(histoTitle);
        EfficiencyVsGeometry::h_EffGeoNr[iden] -> GetXaxis() -> SetTitle("Geometry");
        EfficiencyVsGeometry::h_EffGeoNr[iden] -> GetXaxis() -> SetRangeUser(-0.5, nGeo+.5);
        EfficiencyVsGeometry::h_EffGeoNr[iden] -> GetYaxis() -> SetTitle("Efficiency");
        EfficiencyVsGeometry::h_EffGeoNr[iden] -> GetYaxis() -> SetRangeUser(EfficiencyVsGeometry::effPlotMinY[iden], 1.01);
        std::sprintf(histoTitle, "eff_all_vs_geo_dut_%i", iden);
        EfficiencyVsGeometry::h_EffGeoNr[iden] ->SetName(histoTitle);
        core->output->drawAndSave(EfficiencyVsGeometry::h_EffGeoNr[iden], "AP", "");

        //Print Info:
        TBLOG(kINFO, "DUT " << iden);
        for (int geometryNr = 0; geometryNr<dut->pixelGeometries.size(); geometryNr++)
        {
            if (dut->pixelGeometries[geometryNr]->isEdgePixel()==true) continue;

            if ( EfficiencyVsGeometry::showRunInfo[iden]!=0)
            {
                for (int irun =0; irun<nRuns;irun++)
                {
			if (tracks[iden][geometryNr][core->tbconfig->rawDataRuns.at(irun)]==0) continue;
                    TBLOG(kINFO, "        Geometry: " << geometryNr << " run: " << core->tbconfig->rawDataRuns.at(irun) << "\t Efficiency: " << std::setw(8) << EfficiencyVsGeometry::eff[iden][geometryNr][core->tbconfig->rawDataRuns.at(irun)] << " +- " << std::setw(9) << std::left << EfficiencyVsGeometry::eeff[iden][geometryNr][core->tbconfig->rawDataRuns.at(irun) ]   << std::right << " tracks: " << tracks[iden][geometryNr][core->tbconfig->rawDataRuns.at(irun)]<<std::setw(0));
                }
            }

		if (nTracks[geometryNr]==0) continue;
            TBLOG(kINFO, "Summary Geometry: " << geometryNr << "\t\t Efficiency: " << std::setw(8) << EfficiencyVsGeometry::eff_allruns[iden][geometryNr] << " +" << std::setw(9) << std::left << EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].second << " -" << std::setw(9) << std::left << EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].first << std::right << " tracks: " << nTracks[geometryNr]<<std::setw(0) );
            TBLOG(kINFO, " " );
        }
    }

	EfficiencyVsGeometry::tracks.clear();
	EfficiencyVsGeometry::hits.clear();
	EfficiencyVsGeometry::eff.clear();
	EfficiencyVsGeometry::eeff.clear();
	EfficiencyVsGeometry::eff_allruns.clear();
	EfficiencyVsGeometry::eeff_allruns.clear();
	EfficiencyVsGeometry::h_EffVsRun.clear();
	EfficiencyVsGeometry::h_EffVsRunFCP.clear();
	EfficiencyVsGeometry::h_EffGeoNr.clear();

	delete[] histoTitle;
}


void EfficiencyVsGeometry::efficiencyPerGeometry(DUT* dut, const TBCore* core)
{
    int iden = dut->iden;

    int nRuns = core->tbconfig->rawDataRuns.size();
    int firstRun = core->tbconfig->rawDataRuns.front();
    int lastRun = core->tbconfig->rawDataRuns.back();

    TH1I* h_tracks = new TH1I("h_tracks","tracks vs runs;run;tracks",nRuns,firstRun-.5,lastRun+.5);
    TH1I* h_hits = new TH1I("h_hits","hits vs runs;run;hits",nRuns,firstRun-.5,lastRun+.5);

    for (int geometryNr = 0; geometryNr<dut->pixelGeometries.size(); geometryNr++)
    {
        // if (dut->pixelGeometries[geometryNr]->isEdgePixel()==true) continue;        

        int nTracks = 0;
        int nHits = 0;
        std::vector<double> effs;
        for (auto currentRun: core->tbconfig->rawDataRuns)
        {
            double tracks = EfficiencyVsGeometry::tracks[iden][geometryNr][currentRun];
            double hits = EfficiencyVsGeometry::hits[iden][geometryNr][currentRun];

            if (tracks == 0)
            {
                EfficiencyVsGeometry::eff[iden][geometryNr][currentRun]  = 0.;
                EfficiencyVsGeometry::eeff[iden][geometryNr][currentRun] = 0.;
            }
            else
            {
                // Calculation of efficiency for each run eff: e_i = hits_i / tracks_i
                // Calculation of statistical error for each run eeff: sigma_i = sqrt{ e_i * (1. - e_i) / tracks_i }
                EfficiencyVsGeometry::eff[iden][geometryNr][currentRun]  = hits / tracks;
                EfficiencyVsGeometry::eeff[iden][geometryNr][currentRun] = TMath::Sqrt(hits / tracks * (1. - hits / tracks) / tracks);
            }
            
            nTracks+=tracks;
            nHits+=hits;
            h_tracks->SetBinContent(currentRun+1-firstRun,tracks);
            h_hits->SetBinContent(currentRun+1-firstRun,hits);

            double eff_run = EfficiencyVsGeometry::eff[iden][geometryNr][currentRun];
            if (eff_run > 0.)
            {
                effs.push_back(eff_run);
            }
            // Calculation of efficiency for each geometry eff_allruns: e = 1 / nTracks * sum{ tracks_i * e_i }
            EfficiencyVsGeometry::eff_allruns[iden][geometryNr] += tracks * eff_run;
        }

        // ToDo: include confidence level in analysis config        
        double lvl = .95;
        EfficiencyVsGeometry::h_EffVsRunFCP[iden][geometryNr] = new TEfficiency(*h_hits, *h_tracks);
        EfficiencyVsGeometry::h_EffVsRunFCP[iden][geometryNr]-> SetStatisticOption(TEfficiency::kFCP);
        EfficiencyVsGeometry::h_EffVsRunFCP[iden][geometryNr]-> SetConfidenceLevel(lvl);

        if (nTracks==0)
        {
            EfficiencyVsGeometry::eff_allruns[iden][geometryNr] = 0.;
            EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].first = 0.;
            EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].second = 0.;
        }
        else
        {
            // Calculation of efficiency for each geometry eff_allruns: e = 1 / nTracks * sum{ tracks_i * e_i }
            EfficiencyVsGeometry::eff_allruns[iden][geometryNr] = EfficiencyVsGeometry::eff_allruns[iden][geometryNr]/nTracks;
            if (effs.size()<=1)
            {
                EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].first = 0.;
                EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].second = 0.;
            }
            else
            {
                sort(effs.begin(), effs.end());

                // searches the index of the first element larger than total eff
                int idx_up = std::upper_bound(effs.begin(), effs.end(), EfficiencyVsGeometry::eff_allruns[iden][geometryNr]) - effs.begin();
                int idx_lo;
                // check if total eff is element of effs
                idx_lo = idx_up-1;
                if (effs[idx_lo] == EfficiencyVsGeometry::eff_allruns[iden][geometryNr])
                {
                    idx_lo = idx_lo-1;
                }
                
                // indices and values of the effs closest to total eff
                // std::cout << idx_lo << " " << idx_up << std::endl;
                // std::cout << effs[idx_lo] << " " << effs[idx_up] << std::endl;

                // calculate new idices, containing the set amount of values
                // n.b.: number of elements = index + 1
                idx_lo = idx_lo - ceil((idx_lo + 1)*lvl - 1);
                idx_up = idx_up + ceil((static_cast<int>(effs.size()) - idx_up)*lvl - 1);

                // indices and values of new boundaries
                // std::cout << idx_lo << " " << idx_up << std::endl;
                // std::cout << effs[idx_lo] << " " << effs[idx_up] << std::endl;

                // calculate distance to total eff
                double dist_lo, dist_up;
                dist_lo = eff_allruns[iden][geometryNr] - effs[idx_lo];
                dist_up = effs[idx_up] - eff_allruns[iden][geometryNr];                        
                // std::cout << "-" << dist_lo << " +" << dist_up << std::endl;

                EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].first = dist_lo;
                EfficiencyVsGeometry::eeff_allruns[iden][geometryNr].second = dist_up;

                // // print all elements of vector
                // for(auto elem : effs)
                // {
                //    std::cout << elem << "\n";
                // }
            }
        }
    }
}
