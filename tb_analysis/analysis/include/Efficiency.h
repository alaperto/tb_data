/* 
 * File:   Efficiency.h
 * Author: daniel
 *
 * Created on 16. Februar 2014, 18:11
 */

#ifndef EFFICIENCY_H
#define	EFFICIENCY_H

#include "TBAnalysis.h"

class Efficiency: public TBAnalysis
{
private:
	std::map<IDEN, int> ntracksraw;
	std::map<IDEN, int> ntracksgood;
	std::map<IDEN, int> ntracksregion;
	std::map<IDEN, int> ntracks;
	std::map<IDEN, int> nhits;
	std::map<IDEN, int> anyhit;

	std::map<IDEN, std::map<int, int> > tracksvscol;
	std::map<IDEN, std::map<int, int> > hitsvscol;
	std::map<IDEN, std::map<int, int> > tracksvsrow;
	std::map<IDEN, std::map<int, int> > hitsvsrow;

	std::map<IDEN, TH1D*> h_effvscolDis;
	std::map<IDEN, TH1D*> h_effvsrowDis;

	std::map<IDEN, TH2D*> h_trackMap;
	std::map<IDEN, TH2D*> h_unmatchedClusterMap;
	std::map<IDEN, TH2D*> h_hitMap;
	std::map<IDEN, TH2D*> h_anyHitMap;
	std::map<IDEN, std::vector<TH2D*> > h_trackPixelMap;
	std::map<IDEN, std::vector<TH2D*> > h_hitPixelMap;
	std::map<IDEN, TH2D*> h_trackPixelBiasMap;
	std::map<IDEN, TH2D*> h_hitPixelBiasMap;
	std::map<IDEN, TH2D*> h_trackPixelReadoutMap;
	std::map<IDEN, TH2D*> h_hitPixelReadoutMap;
	std::map<IDEN, TH2D*> h_effMap;

	std::map<IDEN, double> effPlotMinY;
	std::map<IDEN, int> pixMapBinSizeX;
	std::map<IDEN, int> pixMapBinSizeY;
	
	void drawToFilePixelMapEff(const TBCore* core, const PixelGeometry* geometry, int iden, const char* histoTitle, const char* histoName, TH2D* h_hitPixelMap, TH2D* h_trackPixelMap);
	void createPixelProjection(const TBCore* core, const PixelGeometry* geometry, const int iden, const char* histoTitle, const char* histoName, TH2D* h_hitPixelMap, TH2D* h_trackPixelMap);
	void createPixelProjectionOverlay(const TBCore* core, const PixelGeometry* geometry, const int iden, const char* histoTitle, const char* histoName, TH2D* h_hitPixelMap, TH2D* h_trackPixelMap);
	void createPixelEfficiencyMap(const TBCore* core, const PixelGeometry* geometry, const int iden, const char* histoTitle, const char* histoName, TH2D* h_hitPixelMap, TH2D* h_trackPixelMap);
	
	void SetAspectStyle(TH1* h);

	/*void drawToFileSensorAspect(const TBCore& core,
			const char* analysisName, const char* histoName,
			const char* drawOpts, TH1* h1, TH1* h2 = NULL, TH1* h3 = NULL,
			TH1* h4 = NULL) const;
	void drawToFilePixelMapEff(const TBCore& core, std::string analysisName,
				   const char* histoName, TH2D* hitPixelMap, TH2D* trackPixelMap, double maxPitchX, double maxPitchY, DUT* dut);
	bool biasElecRow(const double& ypos, const Event &event);
	bool readoutElecRow(const double& ypos, const Event &event);
	void drawToFileElecEffComp(const TBCore& core, const char* analysisName,
			const char* histoName, TH2D* hitPixelReadMap,
			TH2D* trackPixelReadMap, TH2D* hitPixelBiasMap,
			TH2D* trackPixelBiasMap);
	
	*/
public:
	Efficiency()
	{ 
		TBAnalysis::name = "Efficiency"; 
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new Efficiency;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* EFFICIENCY_H */

