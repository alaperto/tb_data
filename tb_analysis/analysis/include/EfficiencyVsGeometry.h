/* 
 * File:   EfficiencyVsGeometry.h
 * Author: mareike
 *
 * Created on 15. August 2017
 */

#ifndef EFFICIENCYVSGEOMETRY_H
#define EFFICIENCYVSGEOMETRY_H

#include "TBAnalysis.h"

class EfficiencyVsGeometry: public TBAnalysis
{
private:
    std::map<IDEN, std::map<int, std::map<int, int> > > tracks;
    std::map<IDEN, std::map<int, std::map<int, int> > > hits;
    std::map<IDEN, std::map<int, std::map<int, double> > > eff;
    std::map<IDEN, std::map<int, std::map<int, double> > > eeff;

    std::map<IDEN, std::map<int, double> > eff_allruns;
    std::map<IDEN, std::map<int, std::pair<double, double> > > eeff_allruns;

    void efficiencyPerGeometry(DUT* dut, const TBCore* core);

    std::map<IDEN, std::map<int, TEfficiency*> > h_EffVsRunFCP;
    std::map<IDEN, std::map<int, TGraphAsymmErrors*> > h_EffVsRun;
    std::map<IDEN, TGraphAsymmErrors*> h_EffGeoNr;

    std::map<IDEN, double> effPlotMinY;
    std::map<IDEN, double> showRunInfo;

public:
    EfficiencyVsGeometry()
    {
        TBAnalysis::name = "EfficiencyVsGeometry";
    }
    virtual void init(const TBCore* core);
    virtual void event(const TBCore* core, const TBEvent* event);
    virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
    return new EfficiencyVsGeometry;
}

extern "C" void destroy(TBAnalysis* tba)
{
    delete tba;
}

#endif /* EFFICIENCYVSGEOMETRY_H */
